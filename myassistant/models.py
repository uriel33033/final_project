from django.db import models
from django.contrib.auth.models import User #使用內建admin資料庫的user table
from django.urls import reverse

# Create your models here.

class List(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=30,null=False)
    content = models.TextField(max_length=30,default=False)
    pub_time = models.DateTimeField(auto_now=True)

