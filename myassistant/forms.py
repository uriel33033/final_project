#_*_ encoding: utf-8 *_*

from typing import Sized
from django import forms
from django.forms.widgets import EmailInput
from . import models
from .models import User
from django.forms import ModelForm, DateInput
from .models import List
from django.contrib.sessions.models import Session
from django.db.models import Model

class RegisterForm(forms.Form):   
    username = forms.CharField(
        label='帳號:', 
        max_length=20,
        widget=forms.TextInput(attrs={'autofocus':True, 'type':'text'}) 
    )  #給予屬性

    password = forms.CharField(
        label='密碼:',
        widget=forms.PasswordInput() 
    )

    email= forms.CharField(
        label='電子信箱',
        widget=forms.EmailInput()
    )

class LoginForm(forms.Form):

    username = forms.CharField(
        label='帳號:', 
        max_length=20,
        widget=forms.TextInput(attrs={'autofocus':True, 'type':'text'}) 
    )  #給予屬性

    password = forms.CharField(
        label='使用者密碼:',
        max_length=20,
        widget=forms.PasswordInput()
    )

class NewForm(forms.Form):

    title = forms.CharField(
        label='提醒事項:', 
        max_length=30,
        widget=forms.TextInput(attrs={'autofocus':True, 'type':'text'})
    )  #給予屬性

    content = forms.CharField(
        label='詳細內容', 
        max_length=200,
        widget=forms.Textarea
    )  #給予屬性    