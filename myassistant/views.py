from django import views
from django.conf.urls import url
from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from myassistant import forms, models #載入forms.py
from django.forms import ModelForm
from django.contrib.auth.models import User #載入內建user資料庫
from django.contrib.auth import authenticate #使用內建admin認證機制
from django.contrib import auth   #使用內建admin認證機制
from django.contrib.auth.decorators import login_required  #使用內建admin認證機制
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.views import generic
from django.urls import reverse
from myassistant.models import List

from django.contrib.sessions.models import Session

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
# Create your views here.

@login_required(login_url='../login/')
def index(request,pid=None,del_re=None):    

    if pid and del_re != None :
        
        try:
            list=models.List.objects.get(id=pid)
            list.delete()
            messages.add_message(request, messages.SUCCESS, '刪除成功')
            return redirect('..')
        except:
            messages.add_message(request, messages.WARNING, '刪除失敗')
            return redirect('..')

    else:          
        lists=models.List.objects.filter(user__username__contains=request.session['username']).order_by('-pub_time')[:150]   #user of List \ username of User Foreginkey

    return render(request,'index.html',locals())


@login_required(login_url='../login/')
def new(request): #還沒寫完

    if request.method == 'POST':
        username = request.session['username'] #讀取目前使用者的名子
        #user=User.objects.get(user=username) 
        #new_list_form=forms.NewForm(request.POST) #讀取註冊資料 

        try:
            if request.POST['title'] !='' : #可以只填title    .is_valid()判斷表單資料都有填寫
                new_title=request.POST['title'] #.strip()去除空白
                new_content=request.POST['content']

                user = models.User.objects.get(username=username)
                new_list=models.List.objects.create(user=user,title=new_title,content=new_content)
                new_list.save()
                #new_list_form.save(user=username)
                messages.add_message(request, messages.SUCCESS, '存檔成功')
                return redirect('..')
        except:
            messages.add_message(request, messages.WARNING, new_list,'未存檔,請重新嘗試')
            return redirect('../new/')
    
    else:
        new_list_form=forms.NewForm()

    return render(request,'new.html',locals())


@login_required(login_url='../login/')
def edit(request,pid=None):    

    if pid != None :
        record=models.List.objects.get(id=pid)

        if request.method=='POST' : #可以只填title    .is_valid()判斷表單資料都有填寫   
            try:
                if request.POST['title'] !='':
                    new_title=request.POST['title'] #.strip()去除空白
                    new_content=request.POST['content']
                    List.objects.filter(id=pid).update(title=new_title,content=new_content)
                    messages.add_message(request, messages.SUCCESS, '修改成功')
                    return redirect('..')
                else:
                    messages.add_message(request, messages.WARNING, '修改失敗')
                    return redirect('..')     
            except:
               messages.add_message(request, messages.WARNING, '寫入失敗')
               return redirect('..')    
                
        else:
            form=forms.NewForm(initial={'title':record.title,'content':record.content,}) #填回紀錄值

    return render(request,'edit.html',locals())


def register(request):
    if request.method == 'POST':
        register_form=forms.RegisterForm(request.POST) #讀取註冊資料

        try:
            if register_form.is_valid():  #.is_valid()判斷表單資料都有填寫
                register_username=request.POST['username'].strip() #去除空白
                register_password=request.POST['password']
                register_email=request.POST['email']

                if User.objects.create_user(username=register_username,password=register_password,email=register_email) : #User.objects.create() 登入認證會失敗
                    messages.add_message(request, messages.SUCCESS, '註冊成功,請重新輸入帳號密碼登入!')
                    return redirect('../login/')
                else:
                    messages.add_message(request, messages.WARNING, '註冊失敗')
                    return redirect('')
        except:
            messages.add_message(request, messages.WARNING, '註冊失敗')
            return redirect('')

    else :
        register_form=forms.RegisterForm()
        
    return render(request,'register.html',locals())

def login(request):

    if request.method == 'POST':
        login_form = forms.LoginForm(request.POST) #讀取資料
        
        if login_form.is_valid():
            login_username=request.POST['username']
            login_password=request.POST['password']
            user = authenticate(username=login_username, password=login_password) #內建認證方法

            if user is not None:
                if user.is_active:
                    auth.login(request, user)
                    messages.add_message(request, messages.SUCCESS, '成功登入了')
                    request.session['username']=user.username    #紀錄session
                    request.session['email']=user.email

                    return redirect('/')
                else:
                    messages.add_message(request, messages.WARNING, '帳號尚未啟用')
            else:
    
                messages.add_message(request, messages.WARNING, '登入失敗,請檢查帳號密碼!')
        else:
            messages.add_message(request, messages.INFO,'請檢查輸入的欄位內容')
    else:
        login_form = forms.LoginForm()#使用forms.py自動長出表格並連接該資料庫

    return render(request,'login.html',locals())


def logout(request):
    auth.logout(request)
    if 'username' in request.session :
        Session.objects.all().delete()
    
    messages.add_message(request, messages.INFO, "成功登出了")

    return redirect('../login/')
