"""final_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from myassistant import views

from django.contrib.auth.models import User #載入內建user資料庫
from django.contrib.auth import authenticate #使用內建admin認證機制
from django.contrib import auth   #使用內建admin認證機制
from django.contrib.auth.decorators import login_required  #使用內建admin認證機制

app_name = 'myassistant'
urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', views.login),
    path('logout/',views.logout),
    path('register/',views.register),
    path('',views.index,name='index'),
    path('<int:pid>/<str:del_re>',views.index),
    path('new/',views.new),
    path('edit/<int:pid>',views.edit),
]
 